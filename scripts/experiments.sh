#!/bin/bash

set -e

# Perform experiments
# $1 - Give source project directory

# $2 - Which benchmarks to perform
# None or 0 - All benchmarks
# 1 - only arithmetic benchmarks
# 2 - only memory latency benchmarks
# 3 - only streaming memory bandwidth benchmarks
# 4 - only memory buffer bandwidth benchmarks
# 5 - only branch penalty benchmarks
# 6 - only kernel overheads benchmarks
# 7 - only memory buffer bandwidth benchmarks for fusion architecture

if [[ -z "$1" ]];
then
	echo "Default directory - ~/workspace/uCLbench used"
	PROJ=~/workspace/uCLbench
else
	echo "Using given directory"
	echo $1
	PROJ=$1
fi

if [[ -z "$2" ]];
then
	echo "Default values used"
	BENCH=0
else
	BENCH=$2
fi

# First, some paths

BUILD=$PROJ/build
BIN=$PROJ/build/bin
RESULT=$PROJ/results

HOST=$(hostname)

# Compile & cd to the directory
pushd .
cd $BUILD
cmake ..
make clean
make
cd $BIN

# Perform arithmetic benchmarks
if [[ BENCH -eq 1 || BENCH -eq 0 ]];
then
	echo "Running arithmetic benchmarks"
	./arith_speed --repeat=10 --csv 2>&1 | tee $RESULT/arith_$HOST.csv
	./arith_speed --repeat=10 --csv --double 2>&1 | tee -a $RESULT/arith_$HOST.csv
	./arith_speed --repeat=10 --csv --native 2>&1 | tee -a $RESULT/arith_$HOST.csv
	./arith_speed --repeat=10 --csv --native --double 2>&1 | tee -a $RESULT/arith_$HOST.csv
fi

# Perform memory latency benchmarks
if [[ BENCH -eq 2 || BENCH -eq 0 ]];
then
	echo "Running memory latency benchmarks"
	./mem_latency --repeat=10 --csv 2>&1 | tee $RESULT/mem_lat_$HOST.csv
fi

# Perform streaming memory bandwith
if [[ BENCH -eq 3 || BENCH -eq 0 ]];
then
	echo "Running streaming memory bandwidth benchmarks"
	./mem_streaming --repeat=10 --csv 2>&1 | tee $RESULT/mem_band_$HOST.csv
fi

# Perform buffer bandwidth
if [[ BENCH -eq 4 || BENCH -eq 0 ]];
then
	echo "Running buffer bandwidth benchmarks"
	./buffer_bandwidth --repeat=10 --csv 2>&1 | tee $RESULT/buff_band_$HOST.csv
	./buffer_bandwidth --repeat=10 --csv --mapped 2>&1 | tee -a $RESULT/buff_band_$HOST.csv
	./buffer_bandwidth --repeat=10 --csv --pinned 2>&1 | tee -a $RESULT/buff_band_$HOST.csv
fi

# Peform branch penalty benchmarks
if [[ BENCH -eq 5 || BENCH -eq 0 ]];
then
	echo "Running branching penalty benchmarks"
	./branch_penalty --repeat=10 --csv 2>&1 | tee $RESULT/branch_penalty_$HOST.csv
fi

# Perform kernel overhead benchmarks
if [[ BENCH -eq 6 || BENCH -eq 0 ]];
then
	echo "Running kernel overhead benchmarks"
	./kernel_overheads --repeat=10 --csv 2>&1 | tee $RESULT/kernel_overheads_$HOST.csv
fi

# Perform buffer bandwidth for fusion architecture
if [[ BENCH -eq 7 || BENCH -eq 0 ]];
then
	cp ../../buffer_bandwidth/BufferBandwidth_Kernels.cl ./
	echo "Running buffer bandwidth benchmarks for FUSION devices"
	./buffer_bandwidth_fusion --repeat=10 --csv 2>&1 | tee $RESULT/buff_band_FUSION_$HOST.csv
fi

popd
echo "*******Experiment completed******"

