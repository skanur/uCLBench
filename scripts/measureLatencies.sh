#!/bin/bash

# Measure unloaded latency of the memory hierarchy
# Edit config file not this!

# Call with two arguments. First start and second specifies end of range. 
# Returns the increment step

declare -A device
declare -A l1cache
declare -A l2cache
declare -A l3cache
declare -A dram
declare -a CORES
declare -a PASSWORD

#######################################################################
assert()                 #  If condition false,
{                         #+ exit from script
                          #+ with appropriate error message.
  E_PARAM_ERR=98
  E_ASSERT_FAILED=99


  if [ -z "$2" ]          #  Not enough parameters passed
  then                    #+ to assert() function.
    return $E_PARAM_ERR   #  No damage done.
  fi

  lineno=$2

  if [ ! $1 ] 
  then
    echo "Assertion failed:  \"$1\""
    echo "File \"$0\", line $lineno"    # Give name of file and line number.
    exit $E_ASSERT_FAILED
  # else
  #   return
  #   and continue executing the script.
  fi  
} # Insert a similar assert() function into a script you need to debug.    

array_contains() {
    local array="$1[@]"
    local seeking=$2
    local in=3
    for element in "${!array}"; do
        if [[ $element == $seeking ]]; then
            in=0
            break
        fi
    done
    return $in
}
#######################################################################

calculateRange() {
    local s=$1
    local e=$2

    local range=$(($(($((e-s))/4))+1))
    if [ $range -lt $intervals ]
    then
        STEP=4
    else
        local multiplier=$((range/intervals))
        STEP=$((multiplier*4))
    fi   
}

setCore() {
    ncores=${#CORES[@]}
    assert "$ncores -gt 0" $LINENO
    local totCores=`lscpu | awk '/^CPU\(s\):/{print $2}'`
    assert "$ncores -le $totCores" $LINENO
    local zeroIndexedCores=$(($totCores-1))
    
    # Set frequency of all cores
    local machine=`uname -n`
    if [ "$DISABLE_FREQ" = false ] ; then
        if [ "$machine" = "odroid" ] ; then
            echo $PASSWORD | sudo -S $SCRIPTDIR/odroid_cpu_freq -s -g powersave -m $FREQ -M $FREQ 
            echo $PASSWORD | sudo -S sh -c "echo 0 > /sys/class/misc/mali0/device/dvfs" # Disable GPU DVFS
            echo $PASSWORD | sudo -S sh -c "echo $GPU_FREQ > /sys/class/misc/mali0/device/dvfs_max_lock" # set max frequency
        else
            echo $PASSWORD | sudo -S cpupower -c all frequency-set -g userspace
            echo $PASSWORD | sudo -S cpupower -c all frequency-set -f $FREQ
        fi
    fi

    # Find unwanted cores from the given list and total cores
    for n in `seq 0 1 $zeroIndexedCores`; do
        array_contains CORES $n
        ret=$?
        if [ $ret -eq 3 ]; then # If not in the array then turn it off
            echo "Turning off CPU-" $n
            echo $PASSWORD | sudo -S sh -c "echo 0 > /sys/devices/system/cpu/cpu$n/online"
        fi
    done
}

resetCores() {
    local totCores=`lscpu | awk '/^CPU\(s\):/{print $2}'`
    local zeroIndexedCores=$(($totCores-1))

    # Turn on GPU dvfs on odroid
    local machine=`uname -n`
    if [ "$machine" = "odroid" ]; then
        echo $PASSWORD | sudo -S $SCRIPTDIR/odroid_cpu_freq -s -g performance -m 1.4G -M 1.4G 
        echo $PASSWORD | sudo -S sh -c "echo 1 > /sys/class/misc/mali0/device/dvfs" # Disable GPU DVFS
        echo $PASSWORD | sudo -S sh -c "echo -1 > /sys/class/misc/mali0/device/dvfs_max_lock" # set max frequency
    fi

    # Enable all the cores
    for n in `seq 1 1 $zeroIndexedCores`; do
        echo "Turning on the CPU-" $n
        echo $PASSWORD | sudo -S sh -c "echo 1 > /sys/devices/system/cpu/cpu$n/online"
    done
}

mem_latency() {
    source config

    mkdir -p ${BUILDDIR}
    pushd . 
    cd ${BUILDDIR}
    rm -rf ./*
    cmake ..
    make
    cd ./bin

    for a in ${accels[@]}; do
        local file=${FILENAME}_${a}.csv
        local start
        local end

        case $1 in
            l1cache)
                start=256
                end=${l1cache[$a]}
                ;;
            l2cache)
                start=${l1cache[$a]}
                end=${l2cache[$a]}
                ;;
            l3cache)
                start=${l2cache[$a]}
                end=${l3cache[$a]}
                ;;
            dram)
                start=${l3cache[$a]}
                end=${dram[$a]}
                ;;
            *)
                echo "Error: paramter not known"
                echo "usage: mem_latency PARAM"
                echo "PARAM=[l1cache|l2cache|l3cache|dram]"
                exit 1
        esac

        calculateRange $start $end

        # Do it first time to get header of csv file
        ./mem_latency --device=${device[$a]} --size=256 --step --csv > temp.csv
        cat temp.csv | sed -n 1,4p >> $file

        setCore
        for wset in `seq $start $STEP $end`; do
            ./mem_latency --device=${device[$a]} --size=$wset --repeats=$ITER --step --csv  > temp.csv 2> >(tee -a $LOGFILE >&2)
            tail -n +5 temp.csv >> $file
            echo "Workset: $wset done" | tee -a $LOGFILE
        done
        echo "Exp on $a ended on `date +\"%d-%m-%y at %H:%M\"`" | tee -a $LOGFILE
        resetCores
    done
    rm -f temp.csv

    popd
}

selectConfig() {
    source config
    local machine=`uname -n`
    case "$machine" in
        sudLap)
            sudLap
            ;;
        sudBeast)
            beast
            ;;
        fusion)
            fusion
            ;;
        odroid)
            odroid
            ;;
        *)
            echo "ERROR"
            exit 1
            ;;
    esac
}

main() {
    selectConfig
    source config

    if [ -z "$PASSWORD" ]; then
        echo "Password env not set. Needed for sudo access"
        exit 1
    fi

    resetCores
    local cores=${#CORES[@]}

    mkdir -p $RESULTSDIR
    local header="# Cores=$cores on CPU at $FREQ, GPU at $GPU_FREQ"
    echo $header > $LOGFILE

    # Measure L1 Cache 
    mem_latency l1cache

    # Measure L2 cache
    mem_latency l2cache

    # Measure L3 cache
    mem_latency l3cache

    # Measure Dram
    mem_latency dram

}

main
