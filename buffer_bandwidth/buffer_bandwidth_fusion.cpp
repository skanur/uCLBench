/*
 *  Custom OpenCL Memory Buffer Bandwidth Benchmark for shared memory architectures
 *
 *  Authors: Sudeep Kanur
 *
 */

#include <map>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#include "clcommon.h"
#include "clinfo.h"

// defines
#define BM_NAME "buffer_bandwidth_fusion"
#define BM_VERSION "1.0"

#define MAX_SIZES 64
#define MAX_WORKITEMS 64
#define DEFAULT_REPEATS 1
#define DEFAULT_ITERATIONS 25
#define TRANSFER_PATHS 6
#define SUCCESS 0
#define FAILURE 1
#define EXPECTED_FAILURE 2
#define ASSERT_CL_RETURN( err )\
   if( (err) != CL_SUCCESS )\
   {\
      fprintf( stderr, "%s:%d: error: %s\n", \
             __FILE__, __LINE__, cluErrorString( (err) ));\
      exit(FAILURE);\
   }

template<typename T> struct _transferRates{
  T writeTime[TRANSFER_PATHS];
  T readTime [TRANSFER_PATHS];
  T mapTime  [TRANSFER_PATHS];
  T unmapTime[TRANSFER_PATHS];
};

typedef _transferRates<double> _transferTimes;
typedef _transferRates<unsigned long> _transferMsecs;

cl_kernel read_kernel, write_kernel; // Read and write kernels
int nThreads, nItemsPerThread, nBytesResult;
const int WS = 128; //Workgroup size
const int nKLoops = 1; //Number of loops inside kernel
cl_uint val = 0;

///////////////////////////////////////////////////////////////////////////////
//  Convert clError into string
///////////////////////////////////////////////////////////////////////////////
const char *cluErrorString(cl_int err) {

   switch(err) {

      case CL_SUCCESS: return "CL_SUCCESS";
      case CL_DEVICE_NOT_FOUND: return "CL_DEVICE_NOT_FOUND";
      case CL_DEVICE_NOT_AVAILABLE: return "CL_DEVICE_NOT_AVAILABLE";
      case CL_COMPILER_NOT_AVAILABLE: return
                                       "CL_COMPILER_NOT_AVAILABLE";
      case CL_MEM_OBJECT_ALLOCATION_FAILURE: return
                                       "CL_MEM_OBJECT_ALLOCATION_FAILURE";
      case CL_OUT_OF_RESOURCES: return "CL_OUT_OF_RESOURCES";
      case CL_OUT_OF_HOST_MEMORY: return "CL_OUT_OF_HOST_MEMORY";
      case CL_PROFILING_INFO_NOT_AVAILABLE: return
                                       "CL_PROFILING_INFO_NOT_AVAILABLE";
      case CL_MEM_COPY_OVERLAP: return "CL_MEM_COPY_OVERLAP";
      case CL_IMAGE_FORMAT_MISMATCH: return "CL_IMAGE_FORMAT_MISMATCH";
      case CL_IMAGE_FORMAT_NOT_SUPPORTED: return
                                       "CL_IMAGE_FORMAT_NOT_SUPPORTED";
      case CL_BUILD_PROGRAM_FAILURE: return "CL_BUILD_PROGRAM_FAILURE";
      case CL_MAP_FAILURE: return "CL_MAP_FAILURE";
      case CL_INVALID_VALUE: return "CL_INVALID_VALUE";
      case CL_INVALID_DEVICE_TYPE: return "CL_INVALID_DEVICE_TYPE";
      case CL_INVALID_PLATFORM: return "CL_INVALID_PLATFORM";
      case CL_INVALID_DEVICE: return "CL_INVALID_DEVICE";
      case CL_INVALID_CONTEXT: return "CL_INVALID_CONTEXT";
      case CL_INVALID_QUEUE_PROPERTIES: return "CL_INVALID_QUEUE_PROPERTIES";
      case CL_INVALID_COMMAND_QUEUE: return "CL_INVALID_COMMAND_QUEUE";
      case CL_INVALID_HOST_PTR: return "CL_INVALID_HOST_PTR";
      case CL_INVALID_MEM_OBJECT: return "CL_INVALID_MEM_OBJECT";
      case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: return
                                       "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
      case CL_INVALID_IMAGE_SIZE: return "CL_INVALID_IMAGE_SIZE";
      case CL_INVALID_SAMPLER: return "CL_INVALID_SAMPLER";
      case CL_INVALID_BINARY: return "CL_INVALID_BINARY";
      case CL_INVALID_BUILD_OPTIONS: return "CL_INVALID_BUILD_OPTIONS";
      case CL_INVALID_PROGRAM: return "CL_INVALID_PROGRAM";
      case CL_INVALID_PROGRAM_EXECUTABLE: return
                                       "CL_INVALID_PROGRAM_EXECUTABLE";
      case CL_INVALID_KERNEL_NAME: return "CL_INVALID_KERNEL_NAME";
      case CL_INVALID_KERNEL_DEFINITION: return "CL_INVALID_KERNEL_DEFINITION";
      case CL_INVALID_KERNEL: return "CL_INVALID_KERNEL";
      case CL_INVALID_ARG_INDEX: return "CL_INVALID_ARG_INDEX";
      case CL_INVALID_ARG_VALUE: return "CL_INVALID_ARG_VALUE";
      case CL_INVALID_ARG_SIZE: return "CL_INVALID_ARG_SIZE";
      case CL_INVALID_KERNEL_ARGS: return "CL_INVALID_KERNEL_ARGS";
      case CL_INVALID_WORK_DIMENSION: return "CL_INVALID_WORK_DIMENSION";
      case CL_INVALID_WORK_GROUP_SIZE: return "CL_INVALID_WORK_GROUP_SIZE";
      case CL_INVALID_WORK_ITEM_SIZE: return "CL_INVALID_WORK_ITEM_SIZE";
      case CL_INVALID_GLOBAL_OFFSET: return "CL_INVALID_GLOBAL_OFFSET";
      case CL_INVALID_EVENT_WAIT_LIST: return "CL_INVALID_EVENT_WAIT_LIST";
      case CL_INVALID_EVENT: return "CL_INVALID_EVENT";
      case CL_INVALID_OPERATION: return "CL_INVALID_OPERATION";
      case CL_INVALID_GL_OBJECT: return "CL_INVALID_GL_OBJECT";
      case CL_INVALID_BUFFER_SIZE: return "CL_INVALID_BUFFER_SIZE";
      case CL_INVALID_MIP_LEVEL: return "CL_INVALID_MIP_LEVEL";
      case CL_INVALID_GLOBAL_WORK_SIZE: return "CL_INVALID_GLOBAL_WORK_SIZE";

      default: return "UNKNOWN CL ERROR CODE";
   }
}

///////////////////////////////////////////////////////////////////////////////
//  Initialize read and write kernels
///////////////////////////////////////////////////////////////////////////////
int initializeKernels(cl_device_id device, cl_context context, cl_command_queue commands, unsigned long size) {
  cl_int err;
  cl_event ev=0;
  
  const int nWorkers = 1; //Number of workers
  const int nWF = 7; //Number of wavefronts
  const int minBytes = WS * sizeof(cl_float) * 4 * nWorkers;
  unsigned long nBytes = size;	//Default size

  const cl_uint deviceMaxComputeUnits = get_max_compute_units(device);

  const cl_uint nItems = nBytes / (4 * sizeof(cl_float));
  nThreads = deviceMaxComputeUnits * WS * nWF;
  if(nThreads > nItems) {
    nThreads = nItems;
  }
  else {
    while (nItems % nThreads != 0){
      nThreads += WS;
    }
  }
  nItemsPerThread = nItems/nThreads;
  nBytesResult = ( nThreads / WS ) * sizeof(cl_float);

  // From BufferBandwidth benchmark of amdSamples
  const unsigned char v = 4 & 0xff;

  for(int i = 0; i < sizeof( cl_float ); i++)
    val |= v << (i * 8);
  
  // Perform runtime source compilation, and obtain kernel entry points.
  const char* kernel_file = "./BufferBandwidth_Kernels.cl";
  FILE *fp = fopen( kernel_file, "rb" );

  if( fp == NULL )
  {
      fprintf( stderr, "%s:%d: can't open kernel file: %s\n", \
	    __FILE__, __LINE__, strerror( errno ));\
      return -1;
  }

  fseek( fp, 0, SEEK_END );
  const size_t kernelSize = ftell( fp );
  const char *kernel_source = (const char *) malloc( kernelSize );

  rewind( fp );
  fread( (void *) kernel_source, 1, kernelSize, fp );

  cl_program program;

  program = clCreateProgramWithSource( context, 1, &kernel_source, &kernelSize, NULL );

  err = clBuildProgram( program, 1, &device, NULL, NULL, NULL );

  static char buf[0x10000]={0};

  clGetProgramBuildInfo( program, device, CL_PROGRAM_BUILD_LOG, 0x10000, buf, NULL );

  ASSERT_CL_RETURN( err )

  read_kernel = clCreateKernel( program, "read_kernel", &err );

  ASSERT_CL_RETURN( err )

  write_kernel = clCreateKernel( program, "write_kernel", &err );

  ASSERT_CL_RETURN( err )
  return err;
}

///////////////////////////////////////////////////////////////////////////////
//  Measure CPU access to local memory 
///////////////////////////////////////////////////////////////////////////////
int measureCPU2Local(cl_context context, cl_command_queue commands, cl_float* data, unsigned long size, 
		unsigned int iterations, _transferMsecs &tMsecs, int index) {
  int err = CL_SUCCESS;
  cl_mem dmem = NULL;		// device memory
  cl_mem hmem = NULL;		// host memory
  cl_float *transfer_data = NULL;
  void *mapped_dmem = NULL;

  // Create device memory in GPU's local memory
  dmem = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_PERSISTENT_MEM_AMD, sizeof(cl_float) * size, NULL, &err);
  if(err != CL_SUCCESS) {
    return err;
  }	
  // Create host memory in CPU local memory
  transfer_data = data;
  clFinish(commands);

  // Record Mapping time
  unsigned long start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i ++) {
    mapped_dmem = clEnqueueMapBuffer(commands, dmem, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_float) * size, 0, NULL, NULL, &err);
    if(err != CL_SUCCESS) {
	      return err;
    }
  }
  clFinish(commands);
  tMsecs.mapTime[index] = elapsed_msecs(start_time);

  // Record write time
  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
      memcpy(mapped_dmem, data, sizeof(cl_float) * size);
  }
  clFinish(commands);
  tMsecs.writeTime[index] = elapsed_msecs(start_time);

  // Record read time
  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
      memcpy(data, mapped_dmem, sizeof(cl_float) * size);
  }
  clFinish(commands);
  tMsecs.readTime[index] = elapsed_msecs(start_time);

  //Record unmap time
  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
  clEnqueueUnmapMemObject(commands, dmem, mapped_dmem, 0, NULL, NULL);
  }
  clFinish(commands);
  tMsecs.unmapTime[index] = elapsed_msecs(start_time);
  
  // get data back and check
  cl_float* check_data = (cl_float*)malloc(size*sizeof(cl_float));
  clEnqueueReadBuffer(commands, dmem, CL_TRUE, 0, size*sizeof(cl_float), check_data, 0, NULL, NULL);
  for(unsigned long i=0; i<size; ++i) {
	  if(check_data[i] != data[i]) {
		  printf("Error transferring data from host to device: %f != %f at index %lu\n", check_data[i], data[i], i);
		  exit(-1);
	  } 
  }
  free(check_data);

  clReleaseMemObject(dmem);

  return err;
}

/////////////////////////////////////////////////////////////////////////////
// Measure CPU access to Uncached memory 
/////////////////////////////////////////////////////////////////////////////
int measureCPU2Uncached(cl_context context, cl_command_queue commands, cl_float* data, unsigned long size, 
		unsigned int iterations, _transferMsecs &tMsecs, int index) {
  int err = CL_SUCCESS;
  cl_mem dmem = NULL;		// device memory
  cl_mem hmem = NULL;		// host memory
  cl_float *transfer_data = NULL;
  void *mapped_dmem = NULL;

  //Create device memory in device local memory
  dmem = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, sizeof(cl_float) * size, NULL, &err);
  if(err != CL_SUCCESS) {
    return err;
  }	
  //Create host memory in CPU local memory
  transfer_data = data;
  clFinish(commands);

  //Record Mapping time
  unsigned long start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
    mapped_dmem = clEnqueueMapBuffer(commands, dmem, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_float) * size, 0, NULL, NULL, &err);
    if(err != CL_SUCCESS) {
	return err;
    }
  }
  clFinish(commands);
  tMsecs.mapTime[index] = elapsed_msecs(start_time);

  //Record Write time
  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
      memcpy(mapped_dmem, data, sizeof(cl_float) * size);
  }
  clFinish(commands);
  tMsecs.writeTime[index] = elapsed_msecs(start_time);

  //Record read time
  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
      memcpy(data, mapped_dmem, sizeof(cl_float) * size);
  }
  clFinish(commands);
  tMsecs.readTime[index] = elapsed_msecs(start_time);

  //Record unmap time
  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
    clEnqueueUnmapMemObject(commands, dmem, mapped_dmem, 0, NULL, NULL);
  }
  clFinish(commands);
  tMsecs.unmapTime[index] = elapsed_msecs(start_time);

  //get data back and check
  cl_float* check_data = (cl_float*)malloc(size*sizeof(cl_float));
  clEnqueueReadBuffer(commands, dmem, CL_TRUE, 0, size*sizeof(cl_float), check_data, 0, NULL, NULL);
  for(unsigned long i=0; i<size; ++i) {
	  if(check_data[i] != data[i]) {
		  printf("Error transferring data from host to device: %f != %f at index %lu\n", check_data[i], data[i], i);
		  exit(-1);
	  } 
  }
  free(check_data);

  clReleaseMemObject(dmem);

  return err;
}

///////////////////////////////////////////////////////////////////////////////
//  Measure CPU access to Cached memory 
///////////////////////////////////////////////////////////////////////////////
int measureCPU2Cached(cl_context context, cl_command_queue commands, cl_float* data, unsigned long size, 
		unsigned int iterations, _transferMsecs &tMsecs, int index) {
  int err = CL_SUCCESS;
  cl_mem dmem = NULL;		// device memory
  cl_mem hmem = NULL;		// host memory
  cl_float *transfer_data = NULL;
  void *mapped_dmem = NULL;

  // Create device memory in device local memory
  dmem = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, sizeof(cl_float) * size, NULL, &err);
  if(err != CL_SUCCESS) {
    return err;
  }	
  // Create host memory in CPU local memory
  transfer_data = data;
  clFinish(commands);

  // Record Mapping time
  unsigned long start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
    mapped_dmem = clEnqueueMapBuffer(commands, dmem, CL_TRUE, CL_MAP_READ, 0, sizeof(cl_float) * size, 0, NULL, NULL, &err);
    if(err != CL_SUCCESS) {
	    return err;
    }
  }
  clFinish(commands);
  tMsecs.mapTime[index] = elapsed_msecs(start_time);

  // Record Write time
  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
      memcpy(mapped_dmem, data, sizeof(cl_float) * size);
  }
  clFinish(commands);
  tMsecs.writeTime[index] = elapsed_msecs(start_time);

  // Record read time
  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
      memcpy(data, mapped_dmem, sizeof(cl_float) * size);
  }
  clFinish(commands);
  tMsecs.readTime[index] = elapsed_msecs(start_time);

  start_time = current_msecs();
  for(unsigned int i = 0; i < iterations; i++) {
    clEnqueueUnmapMemObject(commands, dmem, mapped_dmem, 0, NULL, NULL);
  }
  clFinish(commands);
  tMsecs.unmapTime[index] = elapsed_msecs(start_time);

  // get data back and check
  cl_float* check_data = (cl_float*)malloc(size*sizeof(cl_float));
  clEnqueueReadBuffer(commands, dmem, CL_TRUE, 0, size*sizeof(cl_float), check_data, 0, NULL, NULL);
  for(unsigned long i=0; i<size; ++i) {
	  if(check_data[i] != data[i]) {
		  printf("Error transferring data from host to device: %f != %f at index %lu\n", check_data[i], data[i], i);
		  exit(-1);
	  } 
  }
  free(check_data);

  clReleaseMemObject(dmem);

  return err;
}

///////////////////////////////////////////////////////////////////////////////
//  Measure GPU access to Local memory 
///////////////////////////////////////////////////////////////////////////////
int measureGPU2Local(cl_context context, cl_command_queue commands, cl_float* data, unsigned long size, 
		unsigned int iterations, _transferMsecs &tMsecs, int index) {
  int err = CL_SUCCESS;
  cl_mem dmem_src = NULL;				// device memory - source
  cl_mem dmem_dst = NULL;				// device memory - destination

  // create device buffer and transfer data 
  dmem_src = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(cl_float) * size, NULL, &err);
  if(err != CL_SUCCESS) {
	  return err;
  }
  dmem_dst = clCreateBuffer(context,  CL_MEM_WRITE_ONLY,  sizeof(cl_float) * size, NULL, &err);
  if(err != CL_SUCCESS) {
	  return err;
  }
  err = clEnqueueWriteBuffer(commands, dmem_src, CL_FALSE, 0, sizeof(cl_float) * size, data, 0, NULL, NULL);
  if(err != CL_SUCCESS) {
	  return err;
  }

  // actual write transfer
  clFinish(commands);
  unsigned long start_time = current_msecs();
  for (unsigned int i = 0; i < iterations; i++) {
	  err = clEnqueueCopyBuffer(commands, dmem_src, dmem_dst, 0, 0, sizeof(cl_float) * size, 0, NULL, NULL);
  }
  clFinish(commands);
  tMsecs.writeTime[index] = elapsed_msecs(start_time);

  // actual read transfer
  clFinish(commands);
  start_time = current_msecs();
  for (unsigned int i = 0; i < iterations; i++) {
	  err = clEnqueueCopyBuffer(commands, dmem_dst, dmem_src, 0, 0, sizeof(cl_float) * size, 0, NULL, NULL);
  }
  clFinish(commands);
  tMsecs.readTime[index] = elapsed_msecs(start_time);

  // get data back and check
  cl_float* check_data = (cl_float*)malloc(size*sizeof(cl_float));
  clEnqueueReadBuffer(commands, dmem_dst, CL_TRUE, 0, size*sizeof(cl_float), check_data, 0, NULL, NULL);
  for(unsigned long i=0; i<size; ++i) {
	  if(check_data[i] != data[i]) {
		  printf("Error transferring data from host to device: %f != %f at index %lu\n", check_data[i], data[i], i);
		  exit(-1);
	  } 
  }
  free(check_data);

  err = clReleaseMemObject(dmem_src);
  err |= clReleaseMemObject(dmem_dst);

  return err;
}

///////////////////////////////////////////////////////////////////////////////
//  Measure GPU access to Uncached memory
///////////////////////////////////////////////////////////////////////////////
int measureGPU2Uncached(cl_device_id device, cl_context context, cl_command_queue commands, cl_float* data, unsigned long size, 
		unsigned int iterations, _transferMsecs &tMsecs, int index) {
  
  cl_int err;
  cl_event ev;
  
  err = initializeKernels(device, context, commands, size);
  ASSERT_CL_RETURN( err )
  
  size_t global_work_size[2] = { nThreads, 0 };
  size_t local_work_size[2] = { WS, 0 };
  
  // Create buffers
  cl_mem dmem = clCreateBuffer(context, CL_MEM_READ_ONLY , sizeof(cl_float) * size, NULL, &err);
  ASSERT_CL_RETURN( err )
  
  cl_mem resultBuffer = clCreateBuffer( context, CL_MEM_READ_WRITE, nBytesResult, NULL, &err);
  ASSERT_CL_RETURN( err )
  
  //Set read kernel and start read measurements
  clSetKernelArg( read_kernel, 0, sizeof(void *),  (void *) &dmem );
  clSetKernelArg( read_kernel, 1, sizeof(void *),  (void *) &resultBuffer );
  clSetKernelArg( read_kernel, 2, sizeof(cl_uint), (void *) &nItemsPerThread);
  clSetKernelArg( read_kernel, 3, sizeof(cl_uint), (void *) &val);
  clSetKernelArg( read_kernel, 4, sizeof(cl_uint), (void *) &nKLoops);
  
  unsigned long start_time = current_msecs();
  for (unsigned int i = 0; i < iterations; i++) {
    err = clEnqueueNDRangeKernel( commands,read_kernel, 1, NULL, global_work_size, local_work_size, 0, NULL, &ev );
    if(err != CL_SUCCESS) {
      printf("\n%d\n at line %d, in file %s", err, __LINE__, __FILE__);
      return err;
    }
    clFinish(commands);
  }
  tMsecs.readTime[index] = elapsed_msecs(start_time);
  
  //Set write kernels and its measurements
  clSetKernelArg( write_kernel, 0, sizeof(void *),  (void *) &resultBuffer );
  clSetKernelArg( write_kernel, 1, sizeof(void *),  (void *) &dmem );
  clSetKernelArg( write_kernel, 2, sizeof(cl_uint), (void *) &nItemsPerThread);
  clSetKernelArg( write_kernel, 3, sizeof(cl_uint), (void *) &val);
  clSetKernelArg( write_kernel, 4, sizeof(cl_uint), (void *) &nKLoops);
  
  start_time = current_msecs();
  for (unsigned int i = 0; i < iterations; i++) {
    err = clEnqueueNDRangeKernel( commands,write_kernel, 1, NULL, global_work_size, local_work_size, 0, NULL, &ev );
    if(err != CL_SUCCESS) {
      printf("\n%d\n at line %d, in file %s", err, __LINE__, __FILE__);
      return err;
    }
    clFinish(commands);
  }
  tMsecs.writeTime[index] = elapsed_msecs(start_time);
  
  clReleaseMemObject(dmem);
  clReleaseMemObject(resultBuffer);
  return err;
}

///////////////////////////////////////////////////////////////////////////////
//  Measure GPU access to Cached memory
///////////////////////////////////////////////////////////////////////////////
int measureGPU2Cached(cl_device_id device, cl_context context, cl_command_queue commands, cl_float* data, unsigned long size, 
		unsigned int iterations, _transferMsecs &tMsecs, int index) {
  
  cl_int err;
  cl_event ev;
  
  err = initializeKernels(device, context, commands, size);
  ASSERT_CL_RETURN( err )
  
  size_t global_work_size[2] = { nThreads, 0 };
  size_t local_work_size[2] = { WS, 0 };
  
  // Create buffers
  cl_mem dmem = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR , sizeof(cl_float) * size, NULL, &err);
  ASSERT_CL_RETURN( err )
  
  cl_mem resultBuffer = clCreateBuffer( context, CL_MEM_READ_WRITE, nBytesResult, NULL, &err);
  ASSERT_CL_RETURN( err )
  
  //Set read kernel and start read measurements
  clSetKernelArg( read_kernel, 0, sizeof(void *),  (void *) &dmem );
  clSetKernelArg( read_kernel, 1, sizeof(void *),  (void *) &resultBuffer );
  clSetKernelArg( read_kernel, 2, sizeof(cl_uint), (void *) &nItemsPerThread);
  clSetKernelArg( read_kernel, 3, sizeof(cl_uint), (void *) &val);
  clSetKernelArg( read_kernel, 4, sizeof(cl_uint), (void *) &nKLoops);
  
  unsigned long start_time = current_msecs();
  for (unsigned int i = 0; i < iterations; i++) {
    err = clEnqueueNDRangeKernel( commands,read_kernel, 1, NULL, global_work_size, local_work_size, 0, NULL, &ev );
    if(err != CL_SUCCESS) {
      printf("\n%d\n at line %d, in file %s", err, __LINE__, __FILE__);
      return err;
    }
    clFinish(commands);
  }
  tMsecs.readTime[index] = elapsed_msecs(start_time);
  
  //Set write kernels and its measurements
  clSetKernelArg( write_kernel, 0, sizeof(void *),  (void *) &resultBuffer );
  clSetKernelArg( write_kernel, 1, sizeof(void *),  (void *) &dmem );
  clSetKernelArg( write_kernel, 2, sizeof(cl_uint), (void *) &nItemsPerThread);
  clSetKernelArg( write_kernel, 3, sizeof(cl_uint), (void *) &val);
  clSetKernelArg( write_kernel, 4, sizeof(cl_uint), (void *) &nKLoops);
  
  start_time = current_msecs();
  for (unsigned int i = 0; i < iterations; i++) {
    err = clEnqueueNDRangeKernel( commands,write_kernel, 1, NULL, global_work_size, local_work_size, 0, NULL, &ev );
    if(err != CL_SUCCESS) {
      printf("\n%d\n at line %d, in file %s", err, __LINE__, __FILE__);
      return err;
    }
    clFinish(commands);
  }
  tMsecs.writeTime[index] = elapsed_msecs(start_time);
  
  clReleaseMemObject(dmem);
  clReleaseMemObject(resultBuffer);
  return err;
}

///////////////////////////////////////////////////////////////////////////////
//  Prepare the benchmark for the given device
///////////////////////////////////////////////////////////////////////////////
int prepare_benchmark(cl_device_id device_id, cl_context *context, cl_command_queue *commands) {
  int err;

  // Create a compute context 
  *context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
  if(!*context)
  {
  printf("Error: Failed to create a compute context!\n");
  return err;
  }

  // Create a command queue
  *commands = clCreateCommandQueue(*context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
  if(!*commands)
  {
  printf("Error: Failed to create a command queue!\n");
  return err;
  }
	  
  // everything was fine
  return CL_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
//  Run the bandwidth benchmark for a single memory-size
///////////////////////////////////////////////////////////////////////////////
int run_benchmark(cl_device_id device, cl_context context, cl_command_queue commands, unsigned long size, unsigned int iterations, _transferTimes &tTime) {
	
  int err;                            // error code returned from api calls
  unsigned long write_msecs[TRANSFER_PATHS], read_msecs[TRANSFER_PATHS];
  _transferMsecs tMsecs = {0};

  // Get the device type 
  cl_device_type deviceType;
  size_t deviceTypeRetSize;
  err = clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(deviceType), &deviceType, &deviceTypeRetSize);
  if( err != CL_SUCCESS) {
    printf("Error: Failed getting device info\n");
    return err;
  }

  unsigned long amount = size / sizeof(cl_float);	// correct value for float
  float* data = (cl_float*) malloc(sizeof(cl_float) * amount);

  // Fill our data set with random floats
  for(long unsigned i = 0; i < amount; i++) {
    data[i] = (cl_float) rand();
  }

  // Run CPU to local memory measurement, only if using GPUs
  if(deviceType == CL_DEVICE_TYPE_GPU) {
    // start with the CPU to Local memory measurement
    err = measureCPU2Local(context, commands, data, amount, iterations, tMsecs, 0);
    if (err != CL_SUCCESS) {
      printf("Error: Failed host -> device! in measureCPU2Local\n");
      return err;
    }
  }

  // measure CPU to uncached memory next
  err = measureCPU2Uncached(context, commands, data, amount, iterations, tMsecs, 1);
  if (err != CL_SUCCESS) {
    printf("Error: Failed host -> device! in measureCPU2Uncached\n");
    return err;
  }

  // measure CPU to cached memory next
  err = measureCPU2Cached(context, commands, data, amount, iterations, tMsecs, 2);
  if (err != CL_SUCCESS) {
    printf("Error: Failed host -> device! in measureCPU2Cached\n");
    return err;
  }

  // measure GPU to Local memory next
  err = measureGPU2Local(context, commands, data, amount, iterations, tMsecs, 3);
  if (err != CL_SUCCESS) {
	  printf("Error: Failed host -> device! in measureGPU2Local\n");
	  return err;
  }

  // measure GPU to uncached memory next
  err = measureGPU2Uncached(device, context, commands, data, size, iterations, tMsecs, 4);
  if (err != CL_SUCCESS) {
    printf("Error: Failed host -> device! in measureGPU2Uncached\n Error - %d\n", err);
    return err;
  }

  // measure GPU to Cached memory next
  err = measureGPU2Cached(device, context, commands, data, size, iterations, tMsecs, 5);
  if (err != CL_SUCCESS) {
    printf("Error: Failed host -> device! in measureGPU2Cached\n");
    return err;
  }

  // return time taken 
  for(int i = 0; i < TRANSFER_PATHS; i++) {
    tTime.writeTime[i] = (tMsecs.writeTime[i]/1000.0) / (double) iterations;
    tTime.readTime[i] = (tMsecs.readTime[i]/1000.0) / (double) iterations;
    tTime.mapTime[i] = (tMsecs.mapTime[i]/1000.0) / (double) iterations;
    tTime.unmapTime[i] = (tMsecs.unmapTime[i]/1000.0) / (double) iterations;
  }

  // Shutdown and cleanup
  free(data);
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
//  Print short program-usage
///////////////////////////////////////////////////////////////////////////////
void print_usage() {
  printf("usage: bandwidth [options]\n");
  printf("\n");
  printf("where options include:\n");
  printf(" --info     print short info on available devices\n");
  printf(" --device=<integer>\n");
  printf("            benchmark given device (integer refers to output of --info)\n");
  printf(" --sizes=<integer-list>\n");
  printf("            perform benchmark with given list of memory-sizes (comma-separated)\n");
  printf(" --repeats=<integer>\n");
  printf("            measure given amount of times and return mean values\n");
  printf(" --iterations=<integer>\n");
  printf("            number of iterations to perform\n");
  printf(" --csv      output results as csv\n");
}


///////////////////////////////////////////////////////////////////////////////
//  MAIN: collect input parameters and run benchmarks
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv) {

  int info_flag = 0;
  int usage_flag = 0;
  int csv_flag = 0;
  int c;
  char *sizes_str = NULL;
  char *devices_str = NULL;
  char *repeat_str = NULL;
  char *iterations_str = NULL;

  while (1) {
    static struct option long_options[] =
    {
      /* These options set a flag. */
      {"info", no_argument,       &info_flag, 1},
      {"help", no_argument,       &usage_flag, 1},
      {"csv",  no_argument,       &csv_flag, 1},
      /* These options don't set a flag.
	We distinguish them by their indices. */
      {"sizes",    optional_argument, 0, 'a'},
      {"device",  optional_argument, 0, 'b'},
      {"repeats",  optional_argument, 0, 'c'},
      {"iterations",  optional_argument, 0, 'd'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long (argc, argv, "a:", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
	    break;

    switch (c) {
      case 0:
	      /* If this option set a flag, do nothing else now. */
	      break;
      case 'a':
	      sizes_str = optarg;
	      break;
      case 'b':
	      devices_str = optarg;
	      break;
      case 'c':
	      repeat_str = optarg;
	      break;
      case 'd':
	      iterations_str = optarg;
	      break;
      case '?':
	      /* getopt_long already printed an error message. */
	      break;
      default:
	      abort ();
    }
  }

  // retrieve devices
  unsigned int num_devices = 0, num_platforms;

  cl_platform_id* platforms = get_all_platforms(&num_platforms, NULL);
  cl_device_id* devices = get_all_devices(platforms, num_platforms, &num_devices);
  free(platforms);

  if(devices == NULL) {
    printf("Error: Failed to create a device group!\n");
    return 1;
  }
	
  if (info_flag) {
    print_device_list(devices, num_devices);
    return 0;
  }
	
  if (usage_flag) {
    print_usage();
    return 0;
  }
	
  // retrieve devices to be benchmarked
  cl_device_id *used_devices = (cl_device_id*) malloc(sizeof(cl_device_id) * num_devices);
  unsigned int used_num_devices = 0;
  if((devices_str == '\0') || (strcmp(devices_str, "all") == 0)) {
    // nothing specified, run benchmark for all devices
    for(unsigned int i = 0; i < num_devices; i++) used_devices[i] = devices[i];
    used_num_devices = num_devices;
  } else {
    // check the given device-numbers and fill up the device-array
    char* ptr;
    used_num_devices = 0;
    ptr = strtok(devices_str, ",");
    while(ptr != NULL) {
      unsigned int id = 0;
      if(sscanf(ptr, "%i", &id) > 0) {
	if(id >= 0 && id < num_devices) {
	  used_devices[used_num_devices] = devices[id];
	  used_num_devices++;
	}
      } else {
	  printf("invalid device-number given (%d)\n", id);
	  return 1;
      }
      ptr = strtok(NULL, ",");
    }
  }
	
  // retrieve memory-sizes to run the benchmark with
  unsigned long* sizes = (unsigned long*) malloc(sizeof(unsigned long) * MAX_SIZES * used_num_devices);
  for (unsigned int i = 0; i < MAX_SIZES * used_num_devices; i++) {
    sizes[i] = 0;
  }
  unsigned int num_sizes = 0;
  if(sizes_str == '\0') {
      // nothing specified, use default
      num_sizes = 1;
      for (unsigned int i = 0; i < used_num_devices; i++) {
	sizes[i * MAX_SIZES] = 128 * 1024 * 1024;
      }
  } else {
  // check given numbers and fill up the device-array
    char* ptr;
    ptr = strtok(sizes_str, ",");
    while(ptr != NULL) {
      unsigned long size = 0;
      unsigned long _s;
      char _m;
      if (sscanf(ptr, "%lu%c", &_s, &_m) == 2) {
	switch (_m) {
	  case 'K': case 'k':
		  size = _s * 1024;
		  break;
	  case 'M': case 'm':
		  size = _s * 1024 * 1024;
		  break;
	  default:
		  printf("invalid size given (%s)\n", ptr);
		  return 1;
	}
      } else if (sscanf(ptr, "%lu", &_s) > 0) {
	      size = _s;
      } else {
	  printf("invalid size given (%s)\n", ptr);
	  return 1;
      }
      for (unsigned int i = 0; i < used_num_devices; i++) {
	sizes[num_sizes + i * MAX_SIZES] = size;
      }
      num_sizes ++;
      ptr = strtok(NULL, ",");
    }		
  }
	
  // retrieve amount of repeats for each data-point
  unsigned int repeats = 0;
  if (repeat_str == '\0') {
    repeats = DEFAULT_REPEATS;
  } else {
    if (sscanf(repeat_str, "%d", &repeats) > 0) {
	    // nothing more to do
    } else {
      printf("invalid number of repeats given (%s)\n", repeat_str);
      return 1;
    }
  }

  // retrieve amount of iterations used for each data-point
  unsigned int iterations = 0;
  if (iterations_str== '\0') {
    iterations = DEFAULT_ITERATIONS;
  } else {
    if (sscanf(iterations_str, "%d", &iterations) > 0) {
	    // nothing more to do
    } else {
      printf("invalid number of iterations given (%s)\n", iterations_str);
      return 1;
    }
  }

  // now after all is defined, start the benchmark(s) and print the results

  cl_context context = NULL;
  cl_command_queue commands = NULL;
  for (unsigned int i = 0; i < used_num_devices; i++) {
    prepare_benchmark(used_devices[i], &context, &commands);
    if(!csv_flag) print_short_device_info(used_devices[i], i);
    else print_csv_device_info(used_devices[i]);

    for (unsigned int j = 0; j < num_sizes; j++) {
      unsigned long size = sizes[j + i * MAX_SIZES];
      _transferTimes tTime = {0.0};
      int err = 0;
      for(unsigned int k = 0; k < repeats; k++) {
	_transferTimes _tTime = {0.0};

	err = run_benchmark(used_devices[i], context, commands, size, iterations, _tTime);
	if(err > 0) {
#pragma omp for
	  for(int p = 0; p < TRANSFER_PATHS; p++) {
	    _tTime.readTime[p] 	= NAN; 
	    _tTime.writeTime[p] 	= NAN;
	    _tTime.mapTime[p] 	= NAN;
	    _tTime.unmapTime[p] 	= NAN;
	  }
	  break;
	}
	for(int p = 0; p < TRANSFER_PATHS; p++) {
	  tTime.readTime[p] 	+= _tTime.readTime[p]; 
	  tTime.writeTime[p] 	+= _tTime.writeTime[p];
	  tTime.mapTime[p] 	+= _tTime.mapTime[p];
	  tTime.unmapTime[p] 	+= _tTime.unmapTime[p];
	}
      }
#pragma omp for
      for(int p = 0; p < TRANSFER_PATHS; p++) {
	tTime.readTime[p] 	/= repeats; 
	tTime.writeTime[p]	/= repeats;
	tTime.mapTime[p]	/= repeats;
	tTime.unmapTime[p]	/= repeats;
      }
      // print results
      const char *transferType[4] = {"Read", "Write", "Map", "UnMap"};
      _transferTimes mbps;
#pragma omp for
      for (int p = 0; p < TRANSFER_PATHS; p++) {
	mbps.writeTime[p]	= (sizes[j + i * MAX_WORKITEMS] / tTime.writeTime[p])/1024/1024;
	mbps.readTime[p] 	= (sizes[j + i * MAX_WORKITEMS] / tTime.readTime[p])/1024/1024;
	mbps.mapTime[p] 	= (sizes[j + i * MAX_WORKITEMS] / tTime.mapTime[p])/1024/1024;
	mbps.unmapTime[p] 	= (sizes[j + i * MAX_WORKITEMS] / tTime.unmapTime[p])/1024/1024;
      }
	    
      //Result strings
      const char *rstr[TRANSFER_PATHS] = {"CPU2Local", "CPU2Uncached", "CPU2Cached", "GPU2Local", "GPU2Uncached", "GPU2Cached"};
      
      for(int tt = 0 ; tt < 4; tt ++){
	double *_mbps; // Abstraction leak
	switch(tt) {
	  case 0: _mbps = &(mbps.readTime[0]); 
		  break;
	  case 1: _mbps = &(mbps.writeTime[0]);; 
		  break;
	  case 2: _mbps = &(mbps.mapTime[0]);; 
		  break;
	  case 3: _mbps = &(mbps.unmapTime[0]);; 
		  break;
	}
      
	if(!csv_flag) {
	  printf("%i/%i %s %12lu Bytes", j+1, num_sizes, transferType[tt], size);
	  for(int p = 0; p < TRANSFER_PATHS; p++) {
	    printf("\t%.2f MB/s (%s) ",_mbps[p], rstr[p]);
	  }
	}
	else {
	  printf("#bm: %s, %s\n", BM_NAME, BM_VERSION);
	  printf("#spec: transfer_type:string, iterations:unsigned, size_bytes:unsigned,\n");
	  for(int p = 0; p < TRANSFER_PATHS; p++) {
	    printf("%s:double, ", rstr[p]);
	  }
	  printf("% 10s,% 8u,% 16lu,", 
		  transferType[tt], iterations, size);
	  for(int p = 0; p < TRANSFER_PATHS; p++) {
	    printf("% 12.2lf, ", _mbps[p]);
	  }
	}
	printf("\n");
	fflush(stdout);
      }
    }
    int err = clReleaseCommandQueue(commands);
    err |= clReleaseContext(context);
    if(err != CL_SUCCESS) {
	    printf("Error freeing queue or device.\n");
	    exit(err);
    }
  }
  
  // finally free up all the used memory-chunks
  free(sizes);
  free(used_devices);
  free(devices);
  
  exit(0);
}
